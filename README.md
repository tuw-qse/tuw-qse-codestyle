# TU Wien QSE Codestyle

This repository contains definitions for various quality assurance tools used for writing clean code.

## Formatter

The ``formatter.xml`` should be used in your IDE (IntelliJ, Eclipse, ...) to format Java code.

## Checkstyle

The ``checkstyle.xml``, supporting _Checkstyle 6.2_ and _maven-checkstyle-plugin 2.16_, should be used in your Checkstyle plugin to ensure correctness of the coding style in the source code.

## PMD

The ``pmd.xml``, supporting _PMD 5.3.5_ and _maven-pmd-plugin 3.6_, should be used in your PMD plugin to ensure correctness of the coding style in the source code.

## Findbugs

The ``findbugs.xml``, supporting _Findbugs 3.0.1_ and _maven-findbugs-plugin 3.0.4_, can be used in your Findbugs plugin to ensure correctness of the coding style in the binaries.
